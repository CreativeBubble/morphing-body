import wblut.math.*;
import wblut.processing.*;
import wblut.hemesh.*;
import wblut.geom.*;
import wblut.core.*;
int number_of_patterns = 300;
int number_of_color_patterns = number_of_patterns;
color[] colors_for_patterns = new color[number_of_color_patterns];
Color_high_low[] colorvalues_get_down_high = new Color_high_low[number_of_color_patterns];
float z = 0;
int counter = 0;
int counting_something = 0;



HE_Mesh mesh;
WB_Render3D render;

void setup() {
  size(1000, 1000, P3D);
  smooth(8);
  fill_color_array();
  fill_color_bool_array();
  /*
  createMesh();
   HE_FaceIterator fitr=mesh.fItr();
   int counter = 0;
   while (fitr.hasNext()) {
   fitr.next().setColor(colors_for_patterns[counter]);
   counter = counter+1;
   }
   render=new WB_Render3D(this);
   */
  print(colorvalues_get_down_high[5%number_of_patterns]);
}

void draw() {
  //counter = 0;
  change_colors_in_array_by_noise();
  build_mesh();



  background(34);
  directionalLight(255, 255, 255, 1, 1, -1);
  directionalLight(127, 127, 127, -1, -1, 1);
  translate(width/2, height/2, -700);
  rotateY(frameCount*0.9f/width*TWO_PI);
  rotateX(frameCount*1f/height*TWO_PI);
  stroke(0);
  render.drawEdges(mesh);
  noStroke();
  render.drawFacesFC(mesh);
  // println(frameCount);

  //   z = z +(2*(noise(frameCount*0.05)-0.5));// z gets samller frame 2000  by frame 6000 -255
  //   z = z +(2*(noise(frameCount*0.05)-0.45));// z gets higher frame 2000  by frame 6000 170
  //   z = z +(2*(noise(frameCount*0.05)-0.43));// z gets higher frame 2000  255
  //  println(z);
}

void fill_color_array() {
  for (int i=0; i < number_of_color_patterns; i++) {
    Color_high_low new_bool = new Color_high_low();
    colorvalues_get_down_high[i] = new_bool;
  }
}

void fill_color_bool_array() {
  for (int i=0; i < number_of_color_patterns; i++) {
    float r = (random(255));
    float g = (random(80));
    float b = int(random(80, 180));
    color c = color(r, g, b);
    colors_for_patterns[i] = (c);
  }
}

void build_mesh() {
  createMesh();
  HE_FaceIterator fitr=mesh.fItr();
  while (fitr.hasNext()) {
    fitr.next().setColor(colors_for_patterns[counter%number_of_color_patterns]);
    counter = counter+1;
  }
  //counter = 0;
  render=new WB_Render3D(this);
}

void change_colors_in_array_by_noise() {
  for (int i=0; i < number_of_color_patterns; i++) {
    color c = colors_for_patterns[i];
    float r = (c >> 16) & 0xFF;  // Faster way of getting red(c)
    float g = (c >> 8) & 0xFF;   // Faster way of getting green(c)
    float b = c & 0xFF;          // Faster way of getting blue(c)
    if (i == 0) {
      print(r + "  " + g + "  "+b);
    }
    //changing colorvalue for red
    if (colorvalues_get_down_high[i].get_r_h() == true) {
      r = r+ (4*((noise(frameCount*0.03+counting_something)-0.5)));
    }
    if (colorvalues_get_down_high[i].get_r_h() == false) {
      r = r+ (4*((noise(frameCount*0.03+counting_something)-0.5)));
    }
      if(r > 250){colorvalues_get_down_high[i].change_r_h();}
      if(r < 10){colorvalues_get_down_high[i].change_r_h();}
    counting_something = counting_something +1;

    //changing colorvalue for green
    if (colorvalues_get_down_high[i].get_g_h() == true) {
    g = g+ (4*((noise(frameCount*0.03+100+counting_something)-0.5)));
    }
    if (colorvalues_get_down_high[i].get_g_h() == false) {
    g = g+ (4*((noise(frameCount*0.03+100+counting_something)-0.5)));
    }
      if(g > 250){colorvalues_get_down_high[i].change_g_h();}
      if(g < 10){colorvalues_get_down_high[i].change_g_h();}
    counting_something = counting_something +1;

    //changing colorvalue for blue
    if (colorvalues_get_down_high[i].get_b_h() == true) {
    b = b+ (4*((noise(frameCount*0.03+50)-0.5)));
    }
    if (colorvalues_get_down_high[i].get_b_h() == false) {
    b = b+ (4*((noise(frameCount*0.03+50)-0.5)));
    }
      if(b > 250){colorvalues_get_down_high[i].change_b_h();}
      if(b < 10){colorvalues_get_down_high[i].change_b_h();}

    if (i == 0) {
      println("   changed to:  "+ r + "  "+ g + "  " + b );
    }
    color new_c = color(r, g, b);
    colors_for_patterns[i] = new_c;
    if (counting_something%29 == 0) {
      counting_something = 0;
    }
  }
}

void createMesh() {
  HEC_Dodecahedron creator=new HEC_Dodecahedron();
  creator.setEdge(number_of_patterns);
  mesh=new HE_Mesh(creator);
  HEM_ChamferCorners cc=new HEM_ChamferCorners().setDistance(50%60);
  mesh.modify(cc);

  mesh.getSelection("chamfer").modify(new HEM_Crocodile().setDistance(60));
}
