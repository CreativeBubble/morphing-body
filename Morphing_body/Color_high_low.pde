public class Color_high_low {
  private boolean r_higher = true;
  private boolean g_higher = true;
  private boolean b_higher = true;

  Color_high_low() {
    r_higher = true;
    g_higher = true;
    b_higher = true;
  }

  //Getter


  boolean get_r_h() {
    return r_higher;
  }

  boolean get_g_h() {
    return this.r_higher;
  }

  boolean get_b_h() {
    return this.r_higher;
  }

  //Changer

  void change_r_h() {
    if (r_higher == true) {
      r_higher = false;
    } else {
      r_higher = true;
    }
    return;
  }

  void change_g_h() {
    if (g_higher == true) {
      g_higher = false;
    } else {
      g_higher = true;
    }
    return;
  }

  void change_b_h() {
    if (b_higher == true) {
      b_higher = false;
    } else {
      b_higher = true;
    }
    return;
  }

  //Setter

  void set_r_h(boolean be_or_not_to_be) {
    r_higher = be_or_not_to_be;
    return;
  }

  void set_g_h(boolean be_or_not_to_be) {
    g_higher = be_or_not_to_be;
    return;
  }

  void set_b_h(boolean be_or_not_to_be) {
    b_higher = be_or_not_to_be;
    return;
  }
}
